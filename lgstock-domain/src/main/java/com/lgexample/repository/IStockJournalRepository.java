package com.lgexample.repository;

/**
 * @Author: lihaonan
 * @Date: 2022/9/15
 */
public interface IStockJournalRepository {

    /**
     * 校验库存是否正确
     *
     * @param stockId stockId
     * @return ture/false
     */
    Boolean checkStockJournal(Long stockId);


}
