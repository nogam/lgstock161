package com.lgexample.repository;

import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 库存Repo
 * @Author dingsheng
 * @Date 2022/9/1 23:55
 * @Version 1.0
 */
public interface IStockRepository {

	/**
	 * 查询购物车锁定库存数量
	 * @param stockId 库存Id
	 * @return
	 */
	Integer findShoppingCartLockCount(Long stockId);

	/**
	 * 查询订单锁定库存数量
	 * @param stockId
	 * @return
	 */
	Integer findOrderLockCount(Long stockId);

	/**
	 * 查询库存总数
	 * @param stockId
	 * @return
	 */
	Integer findCount(Long stockId);

	/**
	 * 查询可用数量
	 * @param stockId
	 * @return
	 */
	Integer findRemainCount(@NotNull Long stockId);



}
