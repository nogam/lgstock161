package com.lgexample.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 库存实体
 * TODO 对于部分属性 如商品ID、卖家ID、仓库ID等，是否需要封装校验规则
 * @Author dingsheng
 * @Date 2022/9/1 23:43
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@ToString
public class Stock implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 库存 ID
	 */
	private Long stockId;
	/**
	 * 商品 ID
	 */
	private Long commodityId;
	/**
	 * 卖家 ID
	 */
	private Long sellerId;
	/**
	 * 仓库 ID
	 */
	private Long storageId;
	/**
	 * 购物车锁定数量
	 */
	private Integer shoppingCartLockCount;
	/**
	 * 订单锁定数量
	 */
	private Integer orderLockCount;
	/**
	 * 剩余数量，指仓库中库存减去购物车锁定和订单锁定后剩下的数量
	 */
	private Integer remainCount;
	/**
	 * 总数，指仓库中的数量，不受锁定数量影响
	 */
	private Integer count;
	/**
	 * 是否上架，0:否，1:是
	 */
	private Integer onShow;

}
