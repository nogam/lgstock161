package dao;

import com.alibaba.fastjson.JSONObject;
import com.lgexample.LgStock161Application;
import com.lgexample.domain.entity.Stock;
import com.lgexample.infrastructure.constants.StockCacheConstants;
import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.redis.RedisPlayer;
import com.lgexample.infrastructure.redis.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/24 00:27
 */
@SpringBootTest(classes = LgStock161Application.class)
@RunWith(SpringRunner.class)
public class RedisTest {

  @Autowired
  private RedisPlayer redisPlayer;

  @Autowired
  private RedisUtil redisUtil;

  @Test
  public void testAdd() {
    for (int i = 0; i < 20; i++) {
      StockDo stockDo = new StockDo();
      stockDo.setStockId((long) i);
      stockDo.setVersion(100);
      redisPlayer.cacheStock(stockDo);
    }
  }

  @Test
  public void testRedisUtil() {
    Stock stock = Stock.builder()
            .stockId(111L)
            .commodityId(222L)
            .count(333)
            .onShow(1)
            .orderLockCount(444)
            .remainCount(555)
            .sellerId(666L)
            .shoppingCartLockCount(777)
            .storageId(888L)
            .build();
    redisUtil.hset(StockCacheConstants.L1_KEY_PREFIX_STOCK + stock.getStockId(), StockCacheConstants.ITEM_STOCK_COUNT, stock.getCount());
    redisUtil.hset(StockCacheConstants.L1_KEY_PREFIX_STOCK + stock.getStockId(), StockCacheConstants.ITEM_STOCK_CART_LOCK_COUNT, stock.getShoppingCartLockCount());
    redisUtil.hset(StockCacheConstants.L1_KEY_PREFIX_STOCK + stock.getStockId(), StockCacheConstants.ITEM_STOCK_ORDER_LOCK_COUNT, stock.getOrderLockCount());
    System.out.println(redisUtil.hget(StockCacheConstants.L1_KEY_PREFIX_STOCK + stock.getStockId(), StockCacheConstants.ITEM_STOCK_COUNT, Integer.class));
  }

  @Test
  public void testRedisUtilIncr() {
    System.out.println(redisUtil.hdecr(StockCacheConstants.L1_KEY_PREFIX_STOCK + "111", StockCacheConstants.ITEM_STOCK_COUNT, 1));
  }
}
