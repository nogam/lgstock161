package dao;

import com.lgexample.LgStock161Application;
import com.lgexample.infrastructure.do_.DictGeneralDo;
import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.service.DictGeneralService;
import com.lgexample.infrastructure.service.StockService;
import java.util.List;
import javax.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/24 00:27
 */
@SpringBootTest(classes = LgStock161Application.class)
@RunWith(SpringRunner.class)
public class ShardingDatabaseTest {

  @Resource
  private StockService stockService;

  @Resource
  private DictGeneralService dictGeneralService;

  @Test
  public void testAddToPart() {
    for (int i = 0; i < 20; i++) {
      StockDo sd = new StockDo();
      sd.setCommodityId((long) i);
      sd.setSellerId((long) i);
      sd.setStorageId((long) i);
      sd.setShoppingCartLockCount(100);
      sd.setOrderLockCount(10);
      sd.setRemainCount(20);
      sd.setCount(20);
      sd.setVersion(1);
      sd.setOnShow(1);
      stockService.save(sd);
    }
  }

  @Test
  public void testSelectFromPart() {
    List<StockDo> stockDos = stockService.selectByIdRange(1562352601255702530L,
        1562352601255702540L);
    System.out.println(stockDos);
  }

  @Test
  public void testAddToNonPart() {
    for (int i = 0; i < 20; i++) {
      DictGeneralDo dict = new DictGeneralDo();
      dict.setDictNote("for test");
      dict.setDictName("job" + i);
      dict.setDictType("test");
      dict.setDictValue(i);
      dictGeneralService.save(dict);
    }
  }
}
