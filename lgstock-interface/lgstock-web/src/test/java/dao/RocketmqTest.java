package dao;

import com.lgexample.LgStock161Application;
import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.rocketmq.RocketmqConstant;
import com.lgexample.infrastructure.rocketmq.RocketmqProducer;
import com.lgexample.infrastructure.rocketmq.SyncCacheLocalStockConsumer;
import com.lgexample.infrastructure.service.CacheService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/24 00:27
 */
@SpringBootTest(classes = LgStock161Application.class)
@RunWith(SpringRunner.class)
public class RocketmqTest {

  @Autowired
  private RocketmqProducer rocketmqProducer;

  @Autowired
  private SyncCacheLocalStockConsumer syncCacheLocalStockConsumer;

  @Autowired
  private CacheService cacheService;


  @Test
  public void testAddToPart() throws InterruptedException {
    for (int i = 0; i < 20; i++) {
      StockDo stockDo = new StockDo();
      stockDo.setStockId(1L);
      stockDo.setVersion(1000);
      rocketmqProducer.sendForSyncCacheLocal(stockDo);
    }

    Thread.sleep(5000);
    StockDo stockDo = cacheService.doCacheTest(1L);
    System.out.println(stockDo);
  }

}
