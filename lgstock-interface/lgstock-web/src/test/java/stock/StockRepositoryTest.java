package stock;

import com.lgexample.LgStock161Application;
import com.lgexample.infrastructure.repository.impl.StockRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * StockRepository测试类
 * @Author dingsheng
 * @Date 2022/9/3 2:25
 * @Version 1.0
 */
@SpringBootTest(classes = LgStock161Application.class)
@RunWith(SpringRunner.class)
public class StockRepositoryTest {

	@Autowired
	private StockRepositoryImpl stockRepository;

	@Test
	public void testFindShoppingCartLockCount() {
		Integer shoppingCartLockCount = stockRepository.findShoppingCartLockCount(111L);
    	System.out.println(shoppingCartLockCount);
		Integer shoppingCartLockCount1 = stockRepository.findShoppingCartLockCount(222L);
		System.out.println(shoppingCartLockCount1);
	}

	@Test
	public void testFindOrderLockCount() {
		Integer shoppingCartLockCount = stockRepository.findOrderLockCount(111L);
		System.out.println(shoppingCartLockCount);
		Integer shoppingCartLockCount1 = stockRepository.findOrderLockCount(222L);
		System.out.println(shoppingCartLockCount1);
	}

	@Test
	public void testFindCount() {
//		Integer shoppingCartLockCount = stockRepository.findCount(111L);
//		System.out.println(shoppingCartLockCount);
		stockRepository.findShoppingCartLockCount(888L);
	}

}
