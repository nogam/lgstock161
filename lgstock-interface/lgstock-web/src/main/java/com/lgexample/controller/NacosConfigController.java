package com.lgexample.controller;

import com.lgexample.infrastructure.nacos.config.DubboMonitorConfig;
import com.lgexample.infrastructure.nacos.config.RedisConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/9/4 18:31
 */
@RestController
@RequestMapping("/config/nacos")
public class NacosConfigController {

  @Autowired
  private RedisConfig redisConfig;
  @Autowired
  private DubboMonitorConfig dubboMonitorConfig;

  @GetMapping("/redis")
  public String getRedisConfig() {
    return redisConfig.toString();
  }

  @GetMapping("/dubbo/monitor")
  public String getDubboMonitorConfig() {
    return dubboMonitorConfig.toString();
  }

}
