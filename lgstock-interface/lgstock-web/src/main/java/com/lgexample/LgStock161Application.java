package com.lgexample;

import com.alibaba.cloud.nacos.NacosConfigBootstrapConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.bootstrap.BootstrapConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/22 21:22
 */
@SpringBootApplication
@EnableCaching
@EnableAsync
@EnableDiscoveryClient
@EnableAspectJAutoProxy(exposeProxy = true, proxyTargetClass = true)
public class LgStock161Application {

  public static void main(String[] args) {
    fastjsonSetSafeMode();
    SpringApplication.run(LgStock161Application.class, args);
  }

  private static void fastjsonSetSafeMode() {
//    ParserConfig.getGlobalInstance().setSafeMode(true);
  }
}
