领域驱动设计Demo
[参考文章](https://yq.aliyun.com/articles/719251)

* DDD 内容
  * DDD 四层结构
    * 接口层（interface）：和用户交互，http，rpc
    * 应用层（application）：业务编排、消息转发、消息校验
    * 领域层（domain）：核心层，对领域内容结构的抽象
    * 基础层（infrastructure）：持久化、通用工具等基础设施
  
  * 参考
    * [1](https://developer.aliyun.com/article/713097)
    * [2](https://developer.aliyun.com/article/715802)
    * [3](https://developer.aliyun.com/article/758292)
    * [4](https://mp.weixin.qq.com/s/w1zqhWGuDPsCayiOgfxk6w)
    * [5](https://developer.aliyun.com/article/783664)

* 统一代码格式化
  使用 google-java-format 代码格式，可以在 idea 中添加插件 google-java-format
