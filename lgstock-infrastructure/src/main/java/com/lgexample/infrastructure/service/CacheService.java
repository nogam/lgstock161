package com.lgexample.infrastructure.service;

import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.do_.StockFreezeDo;
import com.lgexample.infrastructure.do_.StockShardDo;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/28 18:54
 */
public interface CacheService {

  StockDo doCache(StockDo stockDo);

  StockFreezeDo doCache(StockFreezeDo stockFreezeDo);

  StockShardDo doCache(StockShardDo stockShardDo);

  StockDo doCacheTest(Long id);
}
