package com.lgexample.infrastructure.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lgexample.infrastructure.do_.StockShardDo;
import com.lgexample.infrastructure.service.StockShardService;
import com.lgexample.infrastructure.mapper.StockShardMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class StockShardServiceImpl extends ServiceImpl<StockShardMapper, StockShardDo>
implements StockShardService{

}




