package com.lgexample.infrastructure.service.impl;

import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.do_.StockFreezeDo;
import com.lgexample.infrastructure.do_.StockShardDo;
import com.lgexample.infrastructure.rocketmq.RocketmqConstant;
import com.lgexample.infrastructure.service.CacheService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/28 18:56
 */
@Component
public class CacheServiceImpl implements CacheService {


  @Cacheable(value = RocketmqConstant.CACHE_NAME_STOCK, key = "#stockDo.stockId")
  public StockDo doCache(StockDo stockDo) {
    return stockDo;
  }

  @Cacheable(value = RocketmqConstant.CACHE_NAME_STOCK_FREEZE, key = "#stockFreezeDo.stockFreezeId")
  public StockFreezeDo doCache(StockFreezeDo stockFreezeDo) {
    return stockFreezeDo;
  }

  @Cacheable(value = RocketmqConstant.CACHE_NAME_STOCK_SHARD, key = "#stockShardDo.stockShardId")
  public StockShardDo doCache(StockShardDo stockShardDo) {
    return stockShardDo;
  }

  @Cacheable(value = RocketmqConstant.CACHE_NAME_STOCK, key = "#id")
  public StockDo doCacheTest(Long id) {
    return null;
  }
}
