package com.lgexample.infrastructure.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lgexample.infrastructure.do_.DictGeneralDo;
import com.lgexample.infrastructure.service.DictGeneralService;
import com.lgexample.infrastructure.mapper.DictGeneralMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class DictGeneralServiceImpl extends ServiceImpl<DictGeneralMapper, DictGeneralDo>
implements DictGeneralService{

}




