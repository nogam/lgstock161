package com.lgexample.infrastructure.service;

import com.lgexample.infrastructure.do_.StockDo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 *
 */
public interface StockService extends IService<StockDo> {
  List<StockDo> selectByIdRange(Long bottomId, Long topId);
}
