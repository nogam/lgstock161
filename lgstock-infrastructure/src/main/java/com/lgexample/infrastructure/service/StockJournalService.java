package com.lgexample.infrastructure.service;

import com.lgexample.infrastructure.do_.StockJournalDo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface StockJournalService extends IService<StockJournalDo> {

}
