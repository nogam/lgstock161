package com.lgexample.infrastructure.service;

import com.lgexample.infrastructure.do_.DictGeneralDo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface DictGeneralService extends IService<DictGeneralDo> {

}
