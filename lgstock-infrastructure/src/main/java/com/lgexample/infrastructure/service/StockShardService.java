package com.lgexample.infrastructure.service;

import com.lgexample.infrastructure.do_.StockShardDo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface StockShardService extends IService<StockShardDo> {

}
