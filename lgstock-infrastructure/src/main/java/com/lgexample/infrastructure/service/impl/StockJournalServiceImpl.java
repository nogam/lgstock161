package com.lgexample.infrastructure.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lgexample.infrastructure.do_.StockJournalDo;
import com.lgexample.infrastructure.service.StockJournalService;
import com.lgexample.infrastructure.mapper.StockJournalMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class StockJournalServiceImpl extends ServiceImpl<StockJournalMapper, StockJournalDo>
implements StockJournalService{

}




