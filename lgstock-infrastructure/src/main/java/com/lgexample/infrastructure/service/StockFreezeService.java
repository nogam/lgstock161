package com.lgexample.infrastructure.service;

import com.lgexample.infrastructure.do_.StockFreezeDo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface StockFreezeService extends IService<StockFreezeDo> {

}
