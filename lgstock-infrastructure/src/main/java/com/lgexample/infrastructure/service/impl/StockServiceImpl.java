package com.lgexample.infrastructure.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.exception.RangeSelectTooLargeException;
import com.lgexample.infrastructure.mapper.StockMapper;
import com.lgexample.infrastructure.service.StockService;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class StockServiceImpl extends ServiceImpl<StockMapper, StockDo> implements StockService {

  private void checkRange(Long bottomId, Long topId) {
    long maxRange = 100L;
    if (topId - bottomId > maxRange) {
      throw new RangeSelectTooLargeException();
    }
  }

  @Override
  public List<StockDo> selectByIdRange(Long bottomId, Long topId) {
    checkRange(topId, bottomId);

    List<Long> collect = Stream.iterate(bottomId, a -> a + 1).limit(topId - bottomId)
        .collect(Collectors.toList());
    return listByIds(collect);
  }
}




