package com.lgexample.infrastructure.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lgexample.infrastructure.do_.StockFreezeDo;
import com.lgexample.infrastructure.service.StockFreezeService;
import com.lgexample.infrastructure.mapper.StockFreezeMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class StockFreezeServiceImpl extends ServiceImpl<StockFreezeMapper, StockFreezeDo>
implements StockFreezeService{

}




