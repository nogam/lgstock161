package com.lgexample.infrastructure.constants;

/**
 * 库存缓存键前缀常量
 * @Author dingsheng
 * @Date 2022/9/3 1:20
 * @Version 1.0
 */
public interface StockCacheConstants {

	public final static String SEPARATOR = "_";

	/**
	 * L1 Cache Key Prefix
	 */
	public final static String L1_KEY_PREFIX_STOCK = "stock_";
	public final static String L1_KEY_PREFIX_STOCK_FREEZE = "stock_freeze_";
	public final static String L1_KEY_PREFIX_STOCK_SHARD = "stock_shard_";

	/** L1 Cache Item **/

	/**
	 * 库存总量
	 */
	public final static String ITEM_STOCK_COUNT = "count";
	/**
	 * 购物车锁定库存总量
	 */
	public final static String ITEM_STOCK_CART_LOCK_COUNT = "cartLockCount";
	/**
	 * 订单锁定库存总量
	 */
	public final static String ITEM_STOCK_ORDER_LOCK_COUNT = "orderLockCount";
	/**
	 * 有效库存总量
	 */
	public final static String ITEM_STOCK_REMAIN_COUNT = "remainCount";

	/** L2 Cache Key prefix **/
	/**
	 * 库存总量
	 */
	public final static String L2_KEY_PREFIX_STOCK_COUNT = L1_KEY_PREFIX_STOCK + ITEM_STOCK_COUNT + SEPARATOR;
	/**
	 * 购物车锁定库存总量
	 */
	public final static String L2_KEY_PREFIX_STOCK_CART_LOCK_COUNT = L1_KEY_PREFIX_STOCK + ITEM_STOCK_CART_LOCK_COUNT + SEPARATOR;
	/**
	 * 订单锁定库存总量
	 */
	public final static String L2_KEY_PREFIX_STOCK_ORDER_LOCK_COUNT = L1_KEY_PREFIX_STOCK + ITEM_STOCK_ORDER_LOCK_COUNT + SEPARATOR;
	/**
	 * 有效库存总量
	 */
	public final static String L2_KEY_PREFIX_STOCK_REMAIN_COUNT = L1_KEY_PREFIX_STOCK + ITEM_STOCK_REMAIN_COUNT + SEPARATOR;

}
