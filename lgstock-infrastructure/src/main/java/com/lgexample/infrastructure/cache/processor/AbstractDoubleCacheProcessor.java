package com.lgexample.infrastructure.cache.processor;

import com.lgexample.infrastructure.enums.L2CacheItemEnum;
import com.lgexample.infrastructure.enums.L2CachePrefixEnum;
import com.lgexample.infrastructure.enums.L1CachePrefixEnum;

/**
 * DoubleCache 自定义二级缓存处理抽象类
 *
 * @author dingsheng
 * @date 2022-09-05
 */
public abstract class AbstractDoubleCacheProcessor {

    /**
     * 前置处理：检查一级缓存是否存在，如果存在直接返回；如果不存在再检查二级缓存，如果二级缓存存在则直接返回，否则查询数据库；
     * @param l1Key
     * @param l2Item
     * @param l2Key
     * @param timeout
     * @return
     */
    abstract Object beforeProcess(L1CachePrefixEnum l1Key, L2CacheItemEnum l2Item, L2CachePrefixEnum l2Key, String key, Long timeout);

    /**
     * 处理:查询数据库
     * @return
     */
    abstract Object process();

    /**
     * 后置处理：更新到一级缓存，二级缓存；
     * @param l1Key
     * @param l2Item
     * @param l2Key
     * @param value
     * @param timeout
     * @return
     */
    abstract Object afterProcess(L1CachePrefixEnum l1Key, L2CacheItemEnum l2Item, L2CachePrefixEnum l2Key, String key, Object value, Long timeout);

}
