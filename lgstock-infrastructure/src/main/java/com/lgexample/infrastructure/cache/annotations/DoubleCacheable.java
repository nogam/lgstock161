package com.lgexample.infrastructure.cache.annotations;

import com.lgexample.infrastructure.cache.processor.AbstractDoubleCacheProcessor;
import com.lgexample.infrastructure.cache.processor.DefaultDoubleCacheProcessor;
import com.lgexample.infrastructure.enums.L2CacheItemEnum;
import com.lgexample.infrastructure.enums.L2CachePrefixEnum;
import com.lgexample.infrastructure.enums.L1CachePrefixEnum;

import javax.validation.constraints.NotBlank;
import java.lang.annotation.*;

/**
 * 自定义二级缓存
 *
 * @author dingsheng
 * @date 2022-09-05
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DoubleCacheable {

    /**
     * 二级缓存键名前缀
     * @return
     */
    @NotBlank
    L2CachePrefixEnum l2KeyPrefix();

    /**
     * 二级缓存属性名
     * @return
     */
    @NotBlank
    L2CacheItemEnum l2Item();

    /**
     * 一级缓存键名
     * @return
     */
    @NotBlank
    L1CachePrefixEnum l1KeyPrefix();

    /**
     * 键
     * @return
     */
    @NotBlank
    String key();

    /**
     * 超时时间秒数，默认600s
     * @return
     */
    long timeout() default 600;

    /**
     * 自定义处理
     * @return
     */
    Class<? extends AbstractDoubleCacheProcessor> process() default DefaultDoubleCacheProcessor.class;

}