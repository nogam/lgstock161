package com.lgexample.infrastructure.cache.aspect;

import com.lgexample.infrastructure.cache.annotations.DoubleCacheable;
import com.lgexample.infrastructure.caffein.CaffeineUtil;
import com.lgexample.infrastructure.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

/**
 * 二级缓存切面类
 *
 * @author dingsheng
 * @date 2022-09-05
 */
@Slf4j
@Aspect
@Component
public class DoubleCacheAspect {

    @Resource
    RedisUtil redisUtil;

    @Resource
    CaffeineUtil caffeineUtil;

    @Pointcut("@annotation(com.lgexample.infrastructure.cache.annotations.DoubleCacheable)")
    private void aroundMethodAspect() {

    }

    @Around(value = "aroundMethodAspect()")
    public Object aroundMethod(ProceedingJoinPoint joinPoint) throws ClassNotFoundException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();
        Method method = signature.getMethod();
        DoubleCacheable handler = method.getAnnotation(DoubleCacheable.class);
        StandardEvaluationContext context = parseElContext(method, args);

        String key = this.getCacheKey(context, handler);
        Optional<Object> res = this.beforeProcess(handler, key);
        if(null == res) {
            return this.process(joinPoint, handler, key);
        }
        if(res.isPresent()) {
            Type genericReturnType = method.getGenericReturnType();
            if (genericReturnType instanceof ParameterizedType) {
                Class<?> rawType = (Class<?>) ((ParameterizedType) genericReturnType).getRawType();
                return redisUtil.getTargetClass(res.get(), rawType);
            }
            return redisUtil.getTargetClass(res.get(), Class.forName(genericReturnType.getTypeName()));
        }
        return null;
    }

    /**
     * 前置处理器:检查缓存有没有
     * @param handler
     * @return Optional<T>
     */
    public Optional<Object> beforeProcess(DoubleCacheable handler, String key) {
        // 检查一级缓存
        Optional<Object> res = Optional.ofNullable(caffeineUtil.caffeineCacheGet(handler.l1KeyPrefix().getCode(), key));
        if(res.isEmpty()) {
            // 检查二级缓存
            res = redisCacheCheck(handler.l2KeyPrefix().getCode(), handler.l2Item().getCode(), key);
            if(null != res && res.isPresent()) {
                // 二级缓存存在，则更新到一级缓存
                caffeineUtil.caffeineCacheSet(handler.l1KeyPrefix().getCode(), key, res.get());
            }
        }
        return res;
    }

    /**
     * 检查一级缓存
     * @param keyPrefix
     * @param keyItem
     * @param key
     * @return 存在返回Optional，不存在返回null
     */
    @SuppressWarnings("all")
    public Optional<Object> redisCacheCheck(String keyPrefix, String keyItem, String key) {
        boolean flag = redisUtil.hHasKey(keyPrefix + key, keyItem);
        if(flag) {
            Object res = redisUtil.hget(keyPrefix + key, keyItem);
            return Optional.ofNullable(res);
        }
        return null;
    }

    /**
     * 缓存中没有，则从数据库中查询，然后写入两级缓存
     * @param joinPoint
     * @return
     */
    public Object process(ProceedingJoinPoint joinPoint, DoubleCacheable handler, String key) {
        Object res;
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        method.setAccessible(true);
        try {
            res = joinPoint.proceed();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
        // 放入两级缓存,为防止缓存穿透，数据库未查到时，将null也缓存起来
        redisUtil.hset(handler.l2KeyPrefix().getCode() + key, handler.l2Item().getCode(), res, handler.timeout());
        caffeineUtil.caffeineCacheSet(handler.l1KeyPrefix().getCode(), key, res);
        return res;
    }

    /**
     * 解析cacheKey
     */
    private String getCacheKey(StandardEvaluationContext context, DoubleCacheable handler) {
        // 使用SpringEL进行key的解析
        ExpressionParser parser = new SpelExpressionParser();
        String redisKeySpringEl = handler.key();
        String res;
        if (redisKeySpringEl.contains("#")) {
            res = parser.parseExpression(redisKeySpringEl).getValue(context, String.class);
        } else {
            res = redisKeySpringEl;
        }
        if(StringUtils.isEmpty(res)) {
            throw new RuntimeException("key is null!");
        }
        return res;
    }

    /**
     * 内容解析
     */
    private StandardEvaluationContext parseElContext(Method method, Object[] args) {
        // 获取被拦截方法参数名列表(使用Spring支持类库)
        LocalVariableTableParameterNameDiscoverer localVariableTable = new LocalVariableTableParameterNameDiscoverer();
        String[] paraNameArr = localVariableTable.getParameterNames(method);
        if (null != paraNameArr && paraNameArr.length > 0) {
            // SpringEL上下文
            StandardEvaluationContext context = new StandardEvaluationContext();
            // 把方法参数放入SpringEL上下文中
            for (int i = 0; i < paraNameArr.length; i++) {
                context.setVariable(paraNameArr[i], args[i]);
            }
            return context;
        }
        return null;
    }

}
