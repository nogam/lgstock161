package com.lgexample.infrastructure.cache.processor;

import com.lgexample.infrastructure.caffein.CaffeineUtil;
import com.lgexample.infrastructure.enums.L2CacheItemEnum;
import com.lgexample.infrastructure.enums.L2CachePrefixEnum;
import com.lgexample.infrastructure.enums.L1CachePrefixEnum;
import com.lgexample.infrastructure.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 二级缓存默认处理器
 *
 * @author dingsheng
 * @date 2022-09-05
 */
public class DefaultDoubleCacheProcessor extends AbstractDoubleCacheProcessor {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    CaffeineUtil caffeineUtil;

    @Override
    Object beforeProcess(L1CachePrefixEnum l1Key, L2CacheItemEnum l2Item, L2CachePrefixEnum l2Key, String key, Long timeout) {
        // 校验参数
        this.check(key);

        Object res = null;
        // 检查一级缓存
        res = caffeineUtil.caffeineCacheGet(l1Key.getCode(), key);
        if(null == res) {
            // 检查二级缓存
            res = redisCacheCheck(l2Key.getCode(), l2Item.getCode(), key);
            if(null != res) {
                // 二级缓存存在，则更新到一级缓存
                caffeineUtil.caffeineCacheSet(l1Key.getCode(), key, res);
            }
        }
        return res;
    }

    @Override
    Object process() {
        return null;
    }

    @Override
    Object afterProcess(L1CachePrefixEnum l1Key, L2CacheItemEnum l2Item, L2CachePrefixEnum l2Key, String key, Object value, Long timeout) {
        if(null != value) {
            // 放入两级缓存
            redisUtil.hset(l2Key.getCode() + key, l2Item.getCode(), value, timeout);
            caffeineUtil.caffeineCacheSet(l1Key.getCode(), key, value);
        }
        return value;
    }

    /**
     * 校验键名不能为空
     * @param key
     */
    private void check(String key) {
        if(null == key) {
            throw new RuntimeException("DoubleCacheProcessor : key cannot be empty!");
        }
    }

    /**
     * 检查二级缓存
     * @param keyPrefix
     * @param keyItem
     * @param key
     * @return
     */
    public Object redisCacheCheck(String keyPrefix, String keyItem, String key) {
        Object res = redisUtil.hget(keyPrefix + key, keyItem);
        return res;
    }
}
