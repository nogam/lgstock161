package com.lgexample.infrastructure.cache.processor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 缓存预处理
 *
 * @author dingsheng
 * @date 2022-09-14
 */
@Slf4j
@Component
public class DefaultPreCacheProcessor extends AbstractPreCacheProcessor {

    /**
     * 缓存预热，将数据预加载到Redis缓存中
     */
    @Async("stockPool")
    @Override
    public void warmupCache() {
        // TODO
        log.info("Warm-Up Cache ......");
    }
}
