package com.lgexample.infrastructure.cache.processor;

/**
 * 缓存预处理
 *
 * @author dingsheng
 * @date 2022-09-14
 */
public abstract class AbstractPreCacheProcessor {

    /**
     * 缓存预热
     */
    public abstract void warmupCache();
}
