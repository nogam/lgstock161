package com.lgexample.infrastructure.redis;

import com.alibaba.fastjson.JSON;
import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.do_.StockFreezeDo;
import com.lgexample.infrastructure.do_.StockShardDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/25 22:17
 */
@Component
public class RedisPlayer {

  private final static String key_prefix_stock = "stock_";
  private final static String key_prefix_stock_freeze = "stock_freeze_";
  private final static String key_prefix_stock_shard = "stock_shard_";
  @Autowired
  private StringRedisTemplate redisTemplate;

  public void cacheStock(StockDo stockDo) {
    Long stockId = stockDo.getStockId();
    String key = key_prefix_stock + stockId;
    redisTemplate.opsForValue().set(key, JSON.toJSONString(stockDo));
  }

  public void cacheStockFreeze(StockFreezeDo stockFreezeDo) {
    Long stockFreezeId = stockFreezeDo.getStockFreezeId();
    String key = key_prefix_stock_freeze + stockFreezeId;
    redisTemplate.opsForValue().set(key, JSON.toJSONString(stockFreezeDo));
  }

  public void cacheStockShard(StockShardDo stockShardDo) {
    Long stockShardId = stockShardDo.getStockShardId();
    String key = key_prefix_stock_shard + stockShardId;
    redisTemplate.opsForValue().set(key, JSON.toJSONString(stockShardDo));
  }
}
