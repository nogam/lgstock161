package com.lgexample.infrastructure.redis.config;

import com.lgexample.infrastructure.cache.processor.AbstractPreCacheProcessor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 缓存预处理
 *
 * @author dingsheng
 * @date 2022-09-14
 */
@Component
public class RedisHandler implements InitializingBean {

    @Autowired
    AbstractPreCacheProcessor preCacheProcessor;

    @Override
    public void afterPropertiesSet() throws Exception {
        // 缓存预热
        preCacheProcessor.warmupCache();
    }
}
