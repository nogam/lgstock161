package com.lgexample.infrastructure.repository.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.do_.StockJournalDo;
import com.lgexample.infrastructure.service.StockJournalService;
import com.lgexample.infrastructure.service.StockService;
import com.lgexample.repository.IStockJournalRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: lihaonan
 * @Date: 2022/10/2
 */
@Slf4j
@Component
public class StockJournalRepositoryImpl implements IStockJournalRepository {

    @Resource
    private StockJournalService stockJournalService;

    @Resource
    private StockService stockService;

    /**
     * 校验库存与库存流水记录
     */
    @Override
    public Boolean checkStockJournal(Long stockId) {
        StockDo byId = stockService.getById(stockId);
        if (byId == null) {
            return null;
        }
        LambdaQueryChainWrapper<StockJournalDo> wrapper = stockJournalService.lambdaQuery().eq(StockJournalDo::getStockId,
                stockId);
        List<StockJournalDo> list = stockJournalService.list(wrapper);
        if (list == null) {
            return null;
        }
        //当前库存是否与流水记录总和相等
        return byId.getCount() == (list.stream().map(StockJournalDo::getCount).count() + byId.getRemainCount());
    }

}
