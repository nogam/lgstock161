package com.lgexample.infrastructure.repository.impl;

import com.lgexample.infrastructure.cache.annotations.DoubleCacheable;
import com.lgexample.infrastructure.enums.L2CacheItemEnum;
import com.lgexample.infrastructure.enums.L2CachePrefixEnum;
import com.lgexample.infrastructure.enums.L1CachePrefixEnum;
import com.lgexample.infrastructure.redis.RedisUtil;
import com.lgexample.repository.IStockRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * 库存Repo实现
 *
 * @author dingsheng
 * @date 2022-09-02
 */
@Slf4j
@Component
@Validated
public class StockRepositoryImpl implements IStockRepository {

    @Autowired
    RedisUtil redisUtil;

    @Override
    @DoubleCacheable(l2KeyPrefix = L2CachePrefixEnum.L2_KEY_PREFIX_STOCK,
            l2Item = L2CacheItemEnum.ITEM_STOCK_CART_LOCK_COUNT,
            l1KeyPrefix = L1CachePrefixEnum.L1_KEY_PREFIX_STOCK_CART_LOCK_COUNT, key = "#stockId", timeout = 600)
    public Integer findShoppingCartLockCount(Long stockId) {
        // TODO 接口参数校验与异常处理

        // 从db查询
        return null;
    }

    @Override
    @DoubleCacheable(l2KeyPrefix = L2CachePrefixEnum.L2_KEY_PREFIX_STOCK,
            l2Item = L2CacheItemEnum.ITEM_STOCK_ORDER_LOCK_COUNT,
            l1KeyPrefix = L1CachePrefixEnum.L1_KEY_PREFIX_STOCK_ORDER_LOCK_COUNT, key = "#stockId")
    public Integer findOrderLockCount(Long stockId) {
        // TODO 接口参数校验与异常处理

        // 从db查询
        return 999;
    }

    @Override
    @DoubleCacheable(l2KeyPrefix = L2CachePrefixEnum.L2_KEY_PREFIX_STOCK,
            l2Item = L2CacheItemEnum.ITEM_STOCK_COUNT,
            l1KeyPrefix = L1CachePrefixEnum.L1_KEY_PREFIX_STOCK_COUNT, key = "#stockId")
    public Integer findCount(Long stockId) {
        // TODO 接口参数校验与异常处理

        // 从db查询
        return 999;
    }

    @Override
    @DoubleCacheable(l2KeyPrefix = L2CachePrefixEnum.L2_KEY_PREFIX_STOCK,
            l2Item = L2CacheItemEnum.ITEM_STOCK_REMAIN_COUNT,
            l1KeyPrefix = L1CachePrefixEnum.L1_KEY_PREFIX_STOCK_REMAIN_COUNT, key = "#stockId")
    public Integer findRemainCount(Long stockId) {
        // TODO 接口参数校验与异常处理

        // 从db查询
        return 999;
    }
}
