package com.lgexample.infrastructure.rocketmq;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 解锁消费者
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/25 19:40
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = RocketmqConstant.UNLOCK, consumerGroup = RocketmqConstant.UNLOCK_CONSUMER_GROUP)
public class UnlockConsumer implements RocketMQListener<String> {



  @Override
  public void onMessage(String message) {

  }
}
