package com.lgexample.infrastructure.rocketmq;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/25 19:44
 */
public class RocketmqConstant {

  public static final String UNLOCK = "UNLOCK";
  public static final String UNFREEZE = "UNFREEZE";
  public static final String SYNC_CACHE_LOCAL = "SYNC_CACHE_LOCAL";
  public static final String SYNC_CACHE_LOCAL_STOCK = "SYNC_CACHE_LOCAL_STOCK";
  public static final String SYNC_CACHE_LOCAL_STOCK_FREEZE = "SYNC_CACHE_LOCAL_STOCK_FREEZE";
  public static final String SYNC_CACHE_LOCAL_STOCK_SHARD = "SYNC_CACHE_LOCAL_STOCK_SHARD";
  public static final String SYNC_CACHE_LOCAL_CONSUMER_GROUP = "SYNC_CACHE_LOCAL_CONSUMER_GROUP";
  public static final String UNFREEZE_CONSUMER_GROUP = "UNFREEZE_SYNC_CACHE_LOCAL_CONSUMER_GROUP";
  public static final String UNLOCK_CONSUMER_GROUP = "UNLOCK_SYNC_CACHE_LOCAL_CONSUMER_GROUP";

  public static final String CACHE_NAME_STOCK = "CACHE_NAME_STOCK";
  public static final String CACHE_NAME_STOCK_FREEZE = "CACHE_NAME_STOCK_FREEZE";
  public static final String CACHE_NAME_STOCK_SHARD = "CACHE_NAME_STOCK_SHARD";
  public static final String CACHE_NAME_DICT_GENERAL = "CACHE_NAME_DICT_GENERAL";

}
