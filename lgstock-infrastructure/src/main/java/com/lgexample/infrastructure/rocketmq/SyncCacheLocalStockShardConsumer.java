package com.lgexample.infrastructure.rocketmq;

import com.alibaba.fastjson.JSON;
import com.lgexample.infrastructure.do_.StockShardDo;
import com.lgexample.infrastructure.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 同步本地缓存 stock 消费者
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/25 19:40
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = RocketmqConstant.SYNC_CACHE_LOCAL,
    selectorExpression = RocketmqConstant.SYNC_CACHE_LOCAL_STOCK_SHARD,
    consumerGroup = RocketmqConstant.SYNC_CACHE_LOCAL_CONSUMER_GROUP)
public class SyncCacheLocalStockShardConsumer implements RocketMQListener<String> {


  @Autowired
  private CacheService cacheService;

  @Override
  public void onMessage(String message) {
    StockShardDo stockShardDo = JSON.parseObject(message, StockShardDo.class);
    cacheService.doCache(stockShardDo);
  }

}
