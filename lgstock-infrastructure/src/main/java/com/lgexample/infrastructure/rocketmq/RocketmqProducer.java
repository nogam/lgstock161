package com.lgexample.infrastructure.rocketmq;

import com.alibaba.fastjson.JSON;
import com.lgexample.infrastructure.do_.StockDo;
import com.lgexample.infrastructure.do_.StockFreezeDo;
import com.lgexample.infrastructure.do_.StockShardDo;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.stereotype.Component;

/**
 * 生产者
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/25 18:39
 */
@Component
@Slf4j
public class RocketmqProducer {

  private final RocketMQTemplate rocketMQTemplate;

  public RocketmqProducer(RocketMQTemplate rocketMQTemplate) {
    this.rocketMQTemplate = rocketMQTemplate;
  }

  public void sendForSyncCacheLocal(StockDo msg) {
    sendForSyncCacheLocal(msg, RocketmqConstant.SYNC_CACHE_LOCAL_STOCK);
  }

  public void sendForSyncCacheLocal(StockFreezeDo msg) {
    sendForSyncCacheLocal(msg, RocketmqConstant.SYNC_CACHE_LOCAL_STOCK_FREEZE);
  }

  public void sendForSyncCacheLocal(StockShardDo msg) {
    sendForSyncCacheLocal(msg, RocketmqConstant.SYNC_CACHE_LOCAL_STOCK_SHARD);
  }

  private void sendForSyncCacheLocal(Object message, String TAG) {
    rocketMQTemplate.convertAndSend(RocketmqConstant.SYNC_CACHE_LOCAL + ":" + TAG,
        JSON.toJSONString(message));
  }

  public void sendForUnlock(Message msg) {
    rocketMQTemplate.convertAndSend(RocketmqConstant.UNLOCK, msg);
  }

  public void sendForUnfreeze(Message msg) {
    rocketMQTemplate.convertAndSend(RocketmqConstant.UNFREEZE, msg);
  }
}
