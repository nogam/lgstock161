package com.lgexample.infrastructure.enums;

/**
 * 通用枚举
 * @Author dingsheng
 * @Date 2022/9/3 1:51
 * @Version 1.0
 */
public enum CommonEnum {
	SEPARATOR("_"),
	;

	private String code;

	CommonEnum(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
