package com.lgexample.infrastructure.enums;

/**
 * 二级缓存属性枚举
 *
 * @author dingsheng
 * @date 2022-09-08
 */
public enum L2CacheItemEnum {

    /** L2 Cache Item **/

    /**
     * 库存总量
     */
    ITEM_STOCK_COUNT("count"),
    /**
     * 购物车锁定库存总量
     */
    ITEM_STOCK_CART_LOCK_COUNT("cartLockCount"),
    /**
     * 订单锁定库存总量
     */
    ITEM_STOCK_ORDER_LOCK_COUNT("orderLockCount"),
    /**
     * 有效库存总量
     */
    ITEM_STOCK_REMAIN_COUNT("remainCount"),
    ;

    private String code;

    L2CacheItemEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}

