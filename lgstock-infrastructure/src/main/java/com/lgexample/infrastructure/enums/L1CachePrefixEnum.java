package com.lgexample.infrastructure.enums;

import com.lgexample.infrastructure.enums.CommonEnum;
import com.lgexample.infrastructure.enums.L2CacheItemEnum;
import com.lgexample.infrastructure.enums.L2CachePrefixEnum;

/**
 * 一级缓存前缀枚举
 *
 * @author dingsheng
 * @date 2022-09-08
 */
public enum L1CachePrefixEnum {

    /** L1 Cache Key prefix **/
    /**
     * 库存总量
     */
    L1_KEY_PREFIX_STOCK_COUNT(L2CachePrefixEnum.L2_KEY_PREFIX_STOCK.getCode()
            + L2CacheItemEnum.ITEM_STOCK_COUNT.getCode()
            + CommonEnum.SEPARATOR.getCode()),
    /**
     * 购物车锁定库存总量
     */
    L1_KEY_PREFIX_STOCK_CART_LOCK_COUNT(L2CachePrefixEnum.L2_KEY_PREFIX_STOCK.getCode()
            + L2CacheItemEnum.ITEM_STOCK_CART_LOCK_COUNT.getCode()
            + CommonEnum.SEPARATOR.getCode()),
    /**
     * 订单锁定库存总量
     */
    L1_KEY_PREFIX_STOCK_ORDER_LOCK_COUNT(L2CachePrefixEnum.L2_KEY_PREFIX_STOCK.getCode()
            + L2CacheItemEnum.ITEM_STOCK_ORDER_LOCK_COUNT.getCode()
            + CommonEnum.SEPARATOR.getCode()),
    /**
     * 有效库存总量
     */
    L1_KEY_PREFIX_STOCK_REMAIN_COUNT(L2CachePrefixEnum.L2_KEY_PREFIX_STOCK.getCode()
            + L2CacheItemEnum.ITEM_STOCK_REMAIN_COUNT.getCode()
            + CommonEnum.SEPARATOR.getCode()),
    ;

    private String code;

    L1CachePrefixEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}

