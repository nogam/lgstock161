package com.lgexample.infrastructure.enums;

/**
 * 二级缓存前缀枚举
 *
 * @author dingsheng
 * @date 2022-09-08
 */
public enum L2CachePrefixEnum {

    /**
     * L2 Cache Key Prefix
     */
    L2_KEY_PREFIX_STOCK("stock_"),
    ;

    private String code;

    L2CachePrefixEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
