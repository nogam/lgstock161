package com.lgexample.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lgexample.infrastructure.do_.StockDo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.lgexample.infrastructure.do_.StockDo
 */
@Mapper
public interface StockMapper extends BaseMapper<StockDo> {

}




