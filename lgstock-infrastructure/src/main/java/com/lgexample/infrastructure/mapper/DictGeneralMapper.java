package com.lgexample.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lgexample.infrastructure.do_.DictGeneralDo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.lgexample.infrastructure.do_.DictGeneralDo
 */
@Mapper
public interface DictGeneralMapper extends BaseMapper<DictGeneralDo> {

}




