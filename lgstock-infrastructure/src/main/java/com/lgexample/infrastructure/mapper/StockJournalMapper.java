package com.lgexample.infrastructure.mapper;

import com.lgexample.infrastructure.do_.StockJournalDo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.lgexample.infrastructure.do_.StockJournalDo
 */
@Mapper
public interface StockJournalMapper extends BaseMapper<StockJournalDo> {

}




