package com.lgexample.infrastructure.mapper;

import com.lgexample.infrastructure.do_.StockFreezeDo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.lgexample.infrastructure.do_.StockFreezeDo
 */
@Mapper
public interface StockFreezeMapper extends BaseMapper<StockFreezeDo> {

}




