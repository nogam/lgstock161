package com.lgexample.infrastructure.mapper;

import com.lgexample.infrastructure.do_.StockShardDo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.lgexample.infrastructure.do_.StockShardDo
 */
@Mapper
public interface StockShardMapper extends BaseMapper<StockShardDo> {

}




