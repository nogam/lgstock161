package com.lgexample.infrastructure.caffein;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * Caffeine配置类
 * @Author dingsheng
 * @Date 2022/9/5 21:56
 * @Version 1.0
 */
@Configuration
public class CaffeineConfig {


	// TODO 修改为从配置文件加载配置
	/*@Bean
	public Caffeine caffeineConfig() {
		Caffeine caffeine = Caffeine.newBuilder()
				.expireAfterWrite(60, TimeUnit.MINUTES)
				.maximumSize(1000);
		return caffeine;
	}

	@Bean
	public CacheManager cacheManager(Caffeine caffeine) {
		CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
		caffeineCacheManager.setCaffeine(caffeine);
		return caffeineCacheManager;
	}*/
}
