package com.lgexample.infrastructure.caffein;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Caffeine工具类
 *
 * @author dingsheng
 * @date 2022-09-08
 */
@Slf4j
@Component
public class CaffeineUtil {

    @Resource
    CaffeineCacheManager cacheManager;

    /**
     * 检查二级缓存
     * @param cacheName
     * @param key
     * @return
     */
    public Object caffeineCacheGet(String cacheName, String key) {
        log.info("caffeine operation:[get][" + cacheName + "][" + key + "]");
        Cache cache = cacheManager.getCache(cacheName);
        Cache.ValueWrapper value = cache.get(key);
        Object res = null;
        if(null != value) {
            res = value.get();
        }
        return res;
    }

    /**
     * 设置二级缓存
     * @param cacheName
     * @param key
     * @param value
     * @return
     */
    public void caffeineCacheSet(String cacheName, String key, Object value) {
        log.info("caffeine operation:[put][" + cacheName + "][" + key + "][" + value + "]");
        Cache cache = cacheManager.getCache(cacheName);
        cache.putIfAbsent(key, value);
    }

}
