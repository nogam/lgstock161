package com.lgexample.infrastructure.do_;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 库存分片表
 *
 * @TableName t_h_stock_shard
 */
@TableName(value = "t_h_stock_shard")
@Data
public class StockShardDo implements Serializable {

  @TableField(exist = false)
  private static final long serialVersionUID = 1L;
  /**
   * 库存分片 ID
   */
  @TableId(type = IdType.ASSIGN_ID)
  private Long stockShardId;
  /**
   * 商品 ID
   */
  private Long commodityId;
  /**
   * 分片 ID，指向分片的 stock_id
   */
  private Long shardId;
  /**
   * 版本号，以实现幂等操作
   */
  private Integer version;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 创建时间
   */
  private Date createTime;

}
