package com.lgexample.infrastructure.do_;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 库存流水表
 *
 * @TableName t_b_stock_journal
 */
@TableName(value = "t_b_stock_journal")
@Data
public class StockJournalDo implements Serializable {

  @TableField(exist = false)
  private static final long serialVersionUID = 1L;
  /**
   * 库存流水 ID
   */
  @TableId(type = IdType.ASSIGN_ID)
  private Long stockJournalId;
  /**
   * 用户 ID，买家或卖家，用操作类型区分
   */
  private Long userId;
  /**
   * 库存 ID
   */
  private Long stockId;
  /**
   * 操作类型，出库，入库，加购物车等操作
   */
  private Integer handleType;
  /**
   * 状态
   */
  private Integer status;
  /**
   * 数量
   */
  private Integer count;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 创建时间
   */
  private Date createTime;
  /**
   * 版本号
   */
  private Integer version;

}
