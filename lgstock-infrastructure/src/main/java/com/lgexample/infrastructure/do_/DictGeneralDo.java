package com.lgexample.infrastructure.do_;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 普通字典表
 *
 * @TableName t_d_dict_general
 */
@TableName(value = "t_d_dict_general")
@Data
public class DictGeneralDo implements Serializable {

  @TableField(exist = false)
  private static final long serialVersionUID = 1L;
  /**
   * 字典 ID
   */
  @TableId(type = IdType.ASSIGN_ID)
  private Long Id;
  /**
   * 字典值
   */
  private Integer dictValue;
  /**
   * 字典值名称
   */
  private String dictName;
  /**
   * 值的描述
   */
  private String dictNote;
  /**
   * 字典类型
   */
  private String dictType;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 创建时间
   */
  private Date createTime;

}
