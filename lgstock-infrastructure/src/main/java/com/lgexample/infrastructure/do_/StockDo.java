package com.lgexample.infrastructure.do_;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 库存表
 *
 * @TableName t_b_stock
 */
@TableName(value = "t_b_stock")
@Data
public class StockDo implements Serializable {

  @TableField(exist = false)
  private static final long serialVersionUID = 1L;
  /**
   * 库存 ID
   */
  @TableId(type = IdType.ASSIGN_ID)
  private Long stockId;
  /**
   * 商品 ID
   */
  private Long commodityId;
  /**
   * 卖家 ID
   */
  private Long sellerId;
  /**
   * 仓库 ID
   */
  private Long storageId;
  /**
   * 购物车锁定数量
   */
  private Integer shoppingCartLockCount;
  /**
   * 订单锁定数量
   */
  private Integer orderLockCount;
  /**
   * 剩余数量，指仓库中库存减去购物车锁定和订单锁定后剩下的数量
   */
  private Integer remainCount;
  /**
   * 总数，指仓库中的数量，不受锁定数量影响
   */
  private Integer count;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 创建时间
   */
  private Date createTime;
  /**
   * 版本号，用来支持幂等操作
   */
  private Integer version;
  /**
   * 是否上架，0:否，1:是
   */
  private Integer onShow;

}
