package com.lgexample.infrastructure.do_;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 库存冻结表，为了支持分时销售，其中冻结开始时间是为了支持冻结商品自动上架展示，冻结结束时间支持自动开始销售
 *
 * @TableName t_b_stock_freeze
 */
@TableName(value = "t_b_stock_freeze")
@Data
public class StockFreezeDo implements Serializable {

  @TableField(exist = false)
  private static final long serialVersionUID = 1L;
  /**
   * 冻结ID
   */
  @TableId(type = IdType.ASSIGN_ID)
  private Long stockFreezeId;
  /**
   * 库存 ID
   */
  private Long stockId;
  /**
   * 冻结开始时间
   */
  private Date startFreezeTime;
  /**
   * 冻结结束时间
   */
  private Date endFreezeTime;
  /**
   * 冻结状态
   */
  private Integer freezeStatus;
  /**
   * 版本号
   */
  private Integer version;

}
