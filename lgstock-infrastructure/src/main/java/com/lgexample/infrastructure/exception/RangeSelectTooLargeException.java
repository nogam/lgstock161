package com.lgexample.infrastructure.exception;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/24 15:57
 */
public class RangeSelectTooLargeException extends RuntimeException {

  public RangeSelectTooLargeException() {
    super("间隔查询的间隔太大");
  }
}
