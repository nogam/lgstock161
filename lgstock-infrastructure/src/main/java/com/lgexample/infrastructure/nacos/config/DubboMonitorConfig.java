package com.lgexample.infrastructure.nacos.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @author MaoLuDong
 * @version 1.0 2022/8/29 20:09
 */
@Component
@RefreshScope
@Data
public class DubboMonitorConfig {

  @Value(value = "${monitor.enable}")
  private Boolean enable;
}
